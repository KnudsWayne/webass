var express = require('express');
var router = express.Router();


// Require our controllers.
var workoutplan_controller = require('../controllers/workoutplanController'); 
var exercise_controller = require('../controllers/exerciseController');


/// WORKOUTPLAN ROUTES ///

// GET catalog home page.
router.get('/', workoutplan_controller.index);  

// GET request for creating a Workoutplan. NOTE This must come before routes that display Workoutplan (uses id).
router.get('/workoutplan/create', workoutplan_controller.workoutplan_create_get);

router.post('/workoutplan/create', workoutplan_controller.workoutplan_create_post);

router.get('/workoutplan/:id', workoutplan_controller.workoutplan_detail)

router.get('/workoutplan/:id/update', workoutplan_controller.workoutplan_update_get)

router.post('/workoutplan/:id/update', workoutplan_controller.workoutplan_update_post)

router.get('/workoutplan', workoutplan_controller.workoutplan_list);



/// EXERCISE ROUTES ///

// GET request for creating Exercise. NOTE This must come before route for id (i.e. display exercise).  
router.get('/exercise/create', exercise_controller.exercise_create_get);

// POST request for creating Exercise
router.post('/exercise/create', exercise_controller.exercise_create_post);

router.get('/exercise', exercise_controller.exercise_list);

module.exports = router;