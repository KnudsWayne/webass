
var mongoose = require('mongoose');

//var dbURI = 'mongodb://localhost/loc8r';

var dbURI = 'mongodb://heroku_gtl0dzcv:6tu6ff75pofo2q05a2149ub32u@ds123783.mlab.com:23783/heroku_gtl0dzcv'




mongoose.connect(dbURI);



mongoose.connection.on('connected', () => {
    console.log('Mongoose connected to ' + dbURI);
});

mongoose.connection.on('error', err => {
    console.log('Mongoose connected error', err);
});

mongoose.connection.on('disconnected', () => {
    console.log('Mongoose disconnected');
});



const gracefulShutdown = (msg, callback) => {
    mongoose.connection.close( () => {
        console.log('Mongoose disconnected through ${msg}');
        callback();
    });
};


process.once('SIGUSR2', () => {
    gracefulShutdown('nodemon restart', () =>{
        process.kill(process.pid, 'SIGUSR2');
    });
});

process.once('SIGINT', () => {
    gracefulShutdown('app termination', () =>{
        process.exit(0);
    });
});

process.once('SIGTERM', () => {
    gracefulShutdown('Heroku app shutdown', () =>{
        process.exit(0);
    });
});